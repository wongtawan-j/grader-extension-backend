# Grader Extension Backend

### Setting Up
- Stellar Account with enough money (or testnet account)
- node.js
- yarn
- influxdb

### Build

- Run influxdb container and open 8086 to localhost

```shell script
docker run -d --name influxdb -p 8086:8086 -e INFLUXDB_DB=grader_extension influxdb:1.8
```

- At root folder of this project, run node.js

```shell script
node src

or 

STELLAR_SECRET_KEY=XXXXXXX node src
```


### Run

### Merkle Tree Backtracing
Please refer to the merkle tree structure in `backtrace.pdf` for a backtracing guide.

### External Call Reference
---
##### POST /open
###### brief
- **use case:** start new quiz session
- this should be called before any grader's login on the quiz
- the system will save the session's description on both Stellar and Influx
- this indicate the quiz starting
###### body
```js
{
    description: string // Description of that quiz
}
```
###### response
- `200`: success
- `500`: unexpected server error, please refer to error message

---

##### POST /close
###### brief
- **use case:** end the quiz session
- this should be called as soon as **everyone** is kicked out from grader
- the sooner this is called, the trustable the system will be
- the system will save all submissions, their respective publicKeys, and their respective student id on both Stellar and Influx
- this indicate the quiz ending
###### body
*[ no request body ]*
###### response
- `200`: success
- `500`: unexpected server error, please refer to error message


---

### Internal Call Reference

### Author
- Wongtawan Junthai
- Punyawat Rujipiranunt
