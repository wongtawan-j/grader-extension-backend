const Express = require('express');
const Router = Express.Router();

const QuizMiddleware = require('../middlewares/quiz');
const AuthenticationMiddleware = require('../middlewares/authentication');

const Base64 = require('../base64.js');
const AssociationRepository = require('../repositories/association');
const SubmissionRepository = require('../repositories/submission');
const QuizRepository = require('../repositories/quiz');
const MerkleTree = require('../merkletree');

const nacl = require('tweetnacl');

class InvalidSubmissionRequestBody extends Error {
    constructor() {
        super('Invalid submission request body');
        this.status = 400;
    }
}

class InvalidInspectRequestBody extends Error {
    constructor() {
        super('Invalid inspect request body');
        this.status = 400;
    }
}

class VerificationFailed extends Error {
    constructor() {
        super('Verification failed');
        this.status = 401;
    }
}

class SubmissionNotFound extends Error {
    constructor() {
        super('Submission not found');
        this.status = 404;
    }
}

class QuizNotFound extends Error {
    constructor() {
        super('Quiz not found');
        this.status = 404;
    }
}

const submit = async (req, res, next) => {
    try {
        const {id, publicKey} = req.authenticationContext;

        const {signedCode} = req.body;
        if (signedCode === undefined) {
            throw new InvalidSubmissionRequestBody();
        }

        const openedCode = nacl.sign.open(Base64.base64ToBytes(signedCode), Base64.base64ToBytes(publicKey));
        if (openedCode === null) {
            throw new VerificationFailed();
        }

        SubmissionRepository.save(id, publicKey, signedCode).catch(error => console.log(error));

        req.logContext.description = `publickey=${publicKey}, signedcode=${signedCode}`;

        res.status(200).send({
            status: 200,
            message: 'save submission successfully',
        });
        next();
    } catch (error) {
        next(error);
    }
}

const inspect = async (req, res, next) => {
    try {
        const {id, code, description} = req.query;
        if (id === undefined || description === undefined) {
            throw new InvalidInspectRequestBody();
        }
        const quiz = await QuizRepository.getTimeInterval(description);

        if (quiz === undefined) {
            throw new QuizNotFound();
        }

        const {startTime, endTime} = quiz;

        const transactionID = await SubmissionRepository.findTransactionByID(startTime, endTime, id);
        const transactionDomain = (process.env.NODE_ENV === 'production') ?
            'https://horizon.stellar.org/transactions' :
            'https://horizon-testnet.stellar.org/transactions'

        if (transactionID === undefined) {
            throw new SubmissionNotFound();
        }

        const submissions = await SubmissionRepository.findSubmissionByID(startTime, endTime, id);
        const merkleTree = MerkleTree.of(...submissions.map(submission => {
            return MerkleTree.of(
                submission.time.getNanoTime(),
                submission.id,
                submission.publicKey,
                submission.signedCode,
            );
        }));

        let matchedCode;
        if (code !== undefined && code !== "") {
            for (const submission of submissions) {
                const openedCode = nacl.sign.open(Base64.base64ToBytes(submission.signedCode), Base64.base64ToBytes(submission.publicKey));
                if (Base64.base64decode(Base64.bytesToBase64(openedCode)) === code) {
                    const associationMerkleTree = MerkleTree.of(id, submission.publicKey);
                    const associationTransactionID = await AssociationRepository.getTransactionID(id, submission.publicKey);
                    const associationMerkleTreeDigest = await associationMerkleTree.merkleDigest();
                    matchedCode = {
                        submission,
                        association: {
                            transaction: `${transactionDomain}/${associationTransactionID}`,
                            root: Buffer.from(associationMerkleTreeDigest.digest, 'hex').toString('base64'),
                            tree: associationMerkleTreeDigest
                        }
                    }
                    break;
                }
            }
            if (matchedCode === undefined) {
                matchedCode = "undefined"
            }
        }

        const merkleTreeDigest = await merkleTree.merkleDigest();

        res.status(200).json({
            matchedCode,
            transaction: `${transactionDomain}/${transactionID}`,
            root: Buffer.from(merkleTreeDigest.digest, 'hex').toString('base64'),
            tree: merkleTreeDigest,
        });
    } catch (error) {
        next(error);
    }
}

Router.post('/save', QuizMiddleware.prepareContext, AuthenticationMiddleware.authenticate, submit)
Router.get('/inspect', inspect);

module.exports = Router;