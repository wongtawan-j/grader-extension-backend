const InfluxDB = require('./datasources/influxdb');
const InfluxSchema = require('./datasources/schema');

exports.save = async (id, publicKey, transactionID) => {
    await InfluxDB.writePoints([{
        measurement: InfluxSchema.Association.measurement,
        fields: {publicKey, transactionID},
        tags: {id},
    }]);
}

exports.findPublicKey = async (id, startTime) => {
    const rows = await InfluxDB.query(`
        select last(publicKey) from ${InfluxSchema.Association.measurement} 
        where id = '${id}' and time >= ${startTime}
    `);
    return (rows.length === 1) ? rows[0].last : undefined;
}

exports.findAllPublicKey = async (id, startTime) => {
    const rows = await InfluxDB.query(`
        select * from ${InfluxSchema.Association.measurement} 
        where id = '${id}' and time >= ${startTime}
    `);
    return rows.map(row => row.publicKey);
}

exports.exists = async (id, publicKey) => {
    const rows = await InfluxDB.query(`
        select * from ${InfluxSchema.Association.measurement}
        where id = '${id}' and publicKey = '${publicKey}'
    `)
    return rows.length !== 0;
}

exports.getTransactionID = async (id, publicKey) => {
    const rows = await InfluxDB.query(`
        select * from ${InfluxSchema.Association.measurement}
        where id = '${id}' and publicKey = '${publicKey}'
    `)
    return rows.length == 0 ? undefined : rows[0].transactionID;
}
