const Influx = require('influx');

exports.Association = {
    measurement: 'association',
    fields: {
        publicKey: Influx.FieldType.STRING,
        transactionID: Influx.FieldType.STRING,
    },
    tags: ['id'],
}

exports.Submission = {
    measurement: 'submission',
    fields: {
        publicKey: Influx.FieldType.STRING,
        signedCode: Influx.FieldType.STRING,
    },
    tags: ['id'],
}

exports.IDSubbmission = {
    measurement: 'id_submission',
    fields: {
        transactionID: Influx.FieldType.STRING,
    },
    tags: ['id']
}

exports.Quiz = {
    measurement: 'quiz',
    fields: {
        status: Influx.FieldType.STRING,
        transactionID: Influx.FieldType.STRING,
    },
    tags: ['description'],
}

exports.Log = {
    measurement: 'log',
    fields: {
        latency: Influx.FieldType.INTEGER,
        description: Influx.FieldType.STRING,
    },
    tags: ['path', 'status', 'id'],
}
