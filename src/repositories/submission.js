const InfluxDB = require('./datasources/influxdb');
const InfluxSchema = require('./datasources/schema');

exports.save = async (id, publicKey, signedCode) => {
    await InfluxDB.writePoints([{
        measurement: InfluxSchema.Submission.measurement,
        fields: {publicKey, signedCode},
        tags: {id},
    }]);
}

exports.findAllSubmission = async (startTime, endTime) => {
    const rows = await InfluxDB.query(`
        select * from ${InfluxSchema.Submission.measurement}
        where time >= ${startTime} and time <= ${endTime}
        group by id order by time
    `);
    return rows;
}

exports.findSubmissionByID = async (startTime, endTime, id) => {
    const rows = await InfluxDB.query(`
        select * from ${InfluxSchema.Submission.measurement}
        where time >= ${startTime} and time <= ${endTime} and id = '${id}'
        order by time
    `);
    return rows;
}

exports.pack = async (id, transactionID) => {
    await InfluxDB.writePoints([{
        measurement: InfluxSchema.IDSubbmission.measurement,
        fields: {transactionID},
        tags: {id},
    }]);
}

exports.findTransactionByID = async (startTime, endTime, id) => {
    const rows = await InfluxDB.query(`
        select * from ${InfluxSchema.IDSubbmission.measurement}
        where time >= ${startTime} and time <= ${endTime} and id = '${id}'
    `);
    return (rows.length !== 0) ? rows[0].transactionID : undefined;
}