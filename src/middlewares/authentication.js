const AssociationRepository = require('../repositories/association');

class InvalidAuthenticationRequestBody extends Error {
    constructor() {
        super("Invalid authentication request body");
        this.status = 400;
    }
}

class InvalidPublicKey extends Error {
    constructor() {
        super("Invalid public key");
        this.status = 401;
    }
}

exports.authenticate = async (req, res, next) => {
    try {
        const {id, publicKey} = req.body;
        if (id === undefined || publicKey === undefined) {
            throw new InvalidAuthenticationRequestBody();
        }
        const currentPublicKey = await AssociationRepository.findPublicKey(id, req.quizContext.startTime);
        if (publicKey !== currentPublicKey) {
            const publicKeyList = await AssociationRepository.findAllPublicKey(id, req.quizContext.startTime);
            const publicKeyIndex = publicKeyList.findIndex((e) => e === publicKey);
            if (publicKeyIndex === -1) {
                throw new InvalidPublicKey();
            }
        }
        req.authenticationContext = {id, publicKey};
        next();
    } catch (error) {
        next(error);
    }
}
